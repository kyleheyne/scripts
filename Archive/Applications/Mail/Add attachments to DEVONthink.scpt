FasdUAS 1.101.10   ��   ��    k             l     ��  ��    A ; Import attachments of selected messages to DEVONthink Pro.     � 	 	 v   I m p o r t   a t t a c h m e n t s   o f   s e l e c t e d   m e s s a g e s   t o   D E V O N t h i n k   P r o .   
  
 l     ��  ��    : 4 Created by Christian Grunenberg on Fri May 18 2012.     �   h   C r e a t e d   b y   C h r i s t i a n   G r u n e n b e r g   o n   F r i   M a y   1 8   2 0 1 2 .      l     ��  ��    4 . Copyright (c) 2012-2013. All rights reserved.     �   \   C o p y r i g h t   ( c )   2 0 1 2 - 2 0 1 3 .   A l l   r i g h t s   r e s e r v e d .      l     ��������  ��  ��        l    � ����  O     �    Q    �     k    �        O    # ! " ! Z   " # $���� # H     % % l    &���� & I   �� '��
�� .coredoexbool       obj  ' 1    ��
�� 
DTcu��  ��  ��   $ R    �� (��
�� .ascrerr ****      � **** ( m     ) ) � * * , N o   d a t a b a s e   i s   i n   u s e .��  ��  ��   " 5    �� +��
�� 
capp + m   	 
 , , � - - @ c o m . d e v o n - t e c h n o l o g i e s . t h i n k p r o 2
�� kfrmID      . / . r   $ ) 0 1 0 l  $ ' 2���� 2 1   $ '��
�� 
slct��  ��   1 o      ���� 0 theselection theSelection /  3 4 3 r   * 3 5 6 5 l  * 1 7���� 7 n   * 1 8 9 8 1   / 1��
�� 
psxp 9 l  * / :���� : I  * /�� ;��
�� .earsffdralis        afdr ; m   * +��
�� afdrtemp��  ��  ��  ��  ��   6 o      ���� 0 	thefolder 	theFolder 4  < = < Z  4 D > ?���� > A  4 9 @ A @ l  4 7 B���� B n   4 7 C D C 1   5 7��
�� 
leng D o   4 5���� 0 theselection theSelection��  ��   A m   7 8����  ? R   < @�� E��
�� .ascrerr ****      � **** E m   > ? F F � G G L O n e   o r   m o r e   m e s s a g e s   m u s t   b e   s e l e c t e d .��  ��  ��   =  H�� H X   E � I�� J I k   W � K K  L M L r   W ` N O N l  W \ P���� P n   W \ Q R Q 1   X \��
�� 
sndr R o   W X���� 0 
themessage 
theMessage��  ��   O o      ���� 0 	thesender 	theSender M  S�� S X   a � T�� U T k   w � V V  W X W r   w � Y Z Y b   w ~ [ \ [ o   w x���� 0 	thefolder 	theFolder \ l  x } ]���� ] n   x } ^ _ ^ 1   y }��
�� 
pnam _ o   x y���� 0 theattachment theAttachment��  ��   Z o      ���� 0 thefile theFile X  ` a ` O  � � b c b I  � ����� d
�� .coresavenull���     obj ��   d �� e��
�� 
kfil e o   � ����� 0 thefile theFile��   c o   � ����� 0 theattachment theAttachment a  f�� f O   � � g h g k   � � i i  j k j r   � � l m l I  � ��� n o
�� .DTpacd01DTrc       utxt n o   � ����� 0 thefile theFile o �� p��
�� 
DTto p 1   � ���
�� 
DTig��   m o      ���� *0 theattachmentrecord theAttachmentRecord k  q�� q r   � � r s r o   � ����� 0 	thesender 	theSender s n       t u t 1   � ���
�� 
pURL u o   � ����� *0 theattachmentrecord theAttachmentRecord��   h 5   � ��� v��
�� 
capp v m   � � w w � x x @ c o m . d e v o n - t e c h n o l o g i e s . t h i n k p r o 2
�� kfrmID  ��  �� 0 theattachment theAttachment U n   d i y z y 2  e i��
�� 
attc z o   d e���� 0 
themessage 
theMessage��  �� 0 
themessage 
theMessage J o   H I���� 0 theselection theSelection��    R      �� { |
�� .ascrerr ****      � **** { o      ���� 0 error_message   | �� }��
�� 
errn } o      ���� 0 error_number  ��    Z  � � ~ ���� ~ >  � � � � � o   � ����� 0 error_number   � m   � �������  I  � ��� � �
�� .sysodisAaleR        TEXT � m   � � � � � � �  M a i l � �� � �
�� 
mesS � o   � ����� 0 error_message   � �� ���
�� 
as A � m   � ���
�� EAlTwarN��  ��  ��    m      � ��                                                                                  emal  alis    H  Mountain Lion              ��iH+   ��Mail.app                                                        �����        ����  	                Applications    �kI      �h�     ��  $Mountain Lion:Applications: Mail.app    M a i l . a p p    M o u n t a i n   L i o n  Applications/Mail.app   / ��  ��  ��     ��� � l     ��������  ��  ��  ��       �� � ���   � ��
�� .aevtoappnull  �   � **** � �� ����� � ���
�� .aevtoappnull  �   � **** � k     � � �  ����  ��  ��   � ���������� 0 
themessage 
theMessage�� 0 theattachment theAttachment�� 0 error_message  �� 0 error_number   � ( ��� ,������ )�������������� F������������������~ w�}�|�{�z�y�x ��w ��v�u�t�s�r
�� 
capp
�� kfrmID  
�� 
DTcu
�� .coredoexbool       obj 
�� 
slct�� 0 theselection theSelection
�� afdrtemp
�� .earsffdralis        afdr
�� 
psxp�� 0 	thefolder 	theFolder
�� 
leng
�� 
kocl
�� 
cobj
�� .corecnte****       ****
�� 
sndr�� 0 	thesender 	theSender
�� 
attc
�� 
pnam�� 0 thefile theFile
� 
kfil
�~ .coresavenull���     obj 
�} 
DTto
�| 
DTig
�{ .DTpacd01DTrc       utxt�z *0 theattachmentrecord theAttachmentRecord
�y 
pURL�x 0 error_message   � �q�p�o
�q 
errn�p 0 error_number  �o  �w��
�v 
mesS
�u 
as A
�t EAlTwarN�s 
�r .sysodisAaleR        TEXT�� �� � �)���0 *�,j  	)j�Y hUO*�,E�O�j 
�,E�O��,k 	)j�Y hO ��[�a l kh  �a ,E` O a�a -[�a l kh ̡a ,%E` O� *a _ l UO)�a �0 !_ a *a ,l E` O_ _ a ,FU[OY��[OY��W &X   �a ! a "a #�a $a %a & 'Y hUascr  ��ޭ