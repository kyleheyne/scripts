FasdUAS 1.101.10   ��   ��    k             l     ��  ��     !/usr/bin/osascript     � 	 	 & ! / u s r / b i n / o s a s c r i p t   
�� 
 i         I     ������
�� .aevtoappnull  �   � ****��  ��    k     �       Z       ����  H        l     ����  F         =        n         1    ��
�� 
prun  m       �                                                                                  emal  alis    H  Macintosh SSD              ���H+     �Mail.app                                                        [�͡4        ����  	                Applications    ��V�      ͡Pt       �  $Macintosh SSD:Applications: Mail.app    M a i l . a p p    M a c i n t o s h   S S D  Applications/Mail.app   / ��    m    ��
�� boovtrue  =       n        1   	 ��
�� 
prun  m    	  �                                                                                      @ alis    h  Macintosh SSD              ���H+     �The Hit List.app                                                �y���        ����  	                Applications    ��V�      ��       �  ,Macintosh SSD:Applications: The Hit List.app  "  T h e   H i t   L i s t . a p p    M a c i n t o s h   S S D  Applications/The Hit List.app   / ��    m    ��
�� boovtrue��  ��    L    ����  ��  ��      ��   O    � ! " ! X    � #�� $ # k   0 � % %  & ' & r   0 6 ( ) ( n  0 4 * + * 4   1 4�� ,
�� 
mbxp , m   2 3 - - � . . 
 I N B O X + o   0 1���� 0 _account   ) o      ���� 
0 _inbox   '  / 0 / r   7 F 1 2 1 l  7 D 3���� 3 N   7 D 4 4 l  7 C 5���� 5 6  7 C 6 7 6 n   7 : 8 9 8 2   8 :��
�� 
mssg 9 o   7 8���� 
0 _inbox   7 =  ; B : ; : 1   < >��
�� 
isfl ; m   ? A��
�� boovtrue��  ��  ��  ��   2 o      ���� 0 	_messages   0  < = < l  G G�� > ?��   > � � We must use this workaround, because the reference will self-update once we unflag a message, and that will get us just one of two flagged messages imported    ? � @ @:   W e   m u s t   u s e   t h i s   w o r k a r o u n d ,   b e c a u s e   t h e   r e f e r e n c e   w i l l   s e l f - u p d a t e   o n c e   w e   u n f l a g   a   m e s s a g e ,   a n d   t h a t   w i l l   g e t   u s   j u s t   o n e   o f   t w o   f l a g g e d   m e s s a g e s   i m p o r t e d =  A B A l  G G�� C D��   C ~ x "Mail got an error: Can�t get item 2 of every message of mailbox..." is the error that applescript reports in this case    D � E E �   " M a i l   g o t   a n   e r r o r :   C a n  t   g e t   i t e m   2   o f   e v e r y   m e s s a g e   o f   m a i l b o x . . . "   i s   t h e   e r r o r   t h a t   a p p l e s c r i p t   r e p o r t s   i n   t h i s   c a s e B  F G F l  G G�� H I��   H � � That error was previously suppressed by the try block (which is weird, because it happens in the repeat), so I've commented it out    I � J J   T h a t   e r r o r   w a s   p r e v i o u s l y   s u p p r e s s e d   b y   t h e   t r y   b l o c k   ( w h i c h   i s   w e i r d ,   b e c a u s e   i t   h a p p e n s   i n   t h e   r e p e a t ) ,   s o   I ' v e   c o m m e n t e d   i t   o u t G  K L K r   G N M N M n   G J O P O 1   H J��
�� 
pcnt P o   G H���� 0 	_messages   N o      ���� 0 _msglist   L  Q�� Q X   O � R�� S R k   a � T T  U V U l  a a�� W X��   W 	 try    X � Y Y  t r y V  Z [ Z r   a x \ ] \ b   a t ^ _ ^ b   a n ` a ` b   a j b c b m   a d d d � e e  H a n d l e   @ e m a i l   " c n   d i f g f 1   e i��
�� 
subj g o   d e���� 0 _message   a m   j m h h � i i  "   f r o m   _ n   n s j k j 1   o s��
�� 
sndr k o   n o���� 0 _message   ] o      ���� 0 
thesubject 
theSubject [  l m l r   y � n o n n   y ~ p q p 1   z ~��
�� 
rdrc q o   y z���� 0 _message   o o      ���� 0 thedate theDate m  r s r r   � � t u t m   � �����   u n       v w v 1   � ���
�� 
hour w o   � ����� 0 thedate theDate s  x y x r   � � z { z m   � �����   { n       | } | 1   � ���
�� 
min  } o   � ����� 0 thedate theDate y  ~  ~ r   � � � � � m   � �����   � n       � � � m   � ���
�� 
scnd � o   � ����� 0 thedate theDate   � � � r   � � � � � b   � � � � � b   � � � � � m   � � � � � � �  m e s s a g e : % 3 c � l  � � ����� � n   � � � � � 1   � ���
�� 
meid � o   � ����� 0 _message  ��  ��   � m   � � � � � � �  % 3 e � o      ���� 0 thebody theBody �  � � � O   � � � � � O  � � � � � I  � ����� �
�� .corecrel****      � null��   � �� � �
�� 
kocl � m   � ���
�� 
Task � �� ���
�� 
prdt � K   � � � � �� � �
�� 
Ttit � o   � ����� 0 
thesubject 
theSubject � �� � �
�� 
Tnts � o   � ����� 0 thebody theBody � �� ���
�� 
Tstd � o   � ����� 0 thedate theDate��  ��   � 1   � ���
�� 
Tinb � m   � � � ��                                                                                      @ alis    h  Macintosh SSD              ���H+     �The Hit List.app                                                �y���        ����  	                Applications    ��V�      ��       �  ,Macintosh SSD:Applications: The Hit List.app  "  T h e   H i t   L i s t . a p p    M a c i n t o s h   S S D  Applications/The Hit List.app   / ��   �  � � � l  � ��� � ���   � , & don't forget to unflag the message :)    � � � � L   d o n ' t   f o r g e t   t o   u n f l a g   t h e   m e s s a g e   : ) �  � � � r   � � � � � m   � ���
�� boovfals � n       � � � 1   � ���
�� 
isfl � o   � ����� 0 _message   �  ��� � l  � ��� � ���   �  end try    � � � �  e n d   t r y��  �� 0 _message   S o   R U���� 0 _msglist  ��  �� 0 _account   $ 2  ! $��
�� 
iact " m     � ��                                                                                  emal  alis    H  Macintosh SSD              ���H+     �Mail.app                                                        [�͡4        ����  	                Applications    ��V�      ͡Pt       �  $Macintosh SSD:Applications: Mail.app    M a i l . a p p    M a c i n t o s h   S S D  Applications/Mail.app   / ��  ��  ��       �� � ���   � ��
�� .aevtoappnull  �   � **** � �� ���� � ���
�� .aevtoappnull  �   � ****��  ��   � ������ 0 _account  �� 0 _message   � ( �� ������������ -���� ��������� d�� h�������������� ��� ���������������������
�� 
prun
�� 
bool
�� 
iact
�� 
kocl
�� 
cobj
�� .corecnte****       ****
�� 
mbxp�� 
0 _inbox  
�� 
mssg �  
�� 
isfl�� 0 	_messages  
�� 
pcnt�� 0 _msglist  
�� 
subj
�� 
sndr�� 0 
thesubject 
theSubject
�� 
rdrc�� 0 thedate theDate
�� 
hour
�� 
min 
�� 
scnd
�� 
meid�� 0 thebody theBody
�� 
Tinb
�� 
Task
�� 
prdt
�� 
Ttit
�� 
Tnts
�� 
Tstd�� �� 
�� .corecrel****      � null�� ���,e 	 	��,e �& hY hO� � �*�-[��l kh  ���/E�O��-�[�,\Ze81E�O��,E` O �_ [��l kh a �a ,%a %�a ,%E` O�a ,E` Oj_ a ,FOj_ a ,FOj_ a ,FOa �a ,%a %E` O� /*a , &*�a  a !a "_ a #_ a $_ a %a & 'UUOf��,FOP[OY�n[OY�8Uascr  ��ޭ